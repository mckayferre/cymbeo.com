$(document).ready(function () {
  $('#dropdownTrigger').click(function (){
    $(this).find('#dropdown').slideToggle('medium');
  });

  // Nav Menu
  $('#menuIcon').click(function (){
    $('#navMobile').show("slide");
  });

  $('#close').click(function (){
    $('#navMobile').slideToggle();
  });


  // Video modal
  // Hide the modal on load
  $('.video-modal').hide();

  // show the modal on click
  $('.activateModal').click(function(){
    $('.video-modal').fadeIn(300).show();
  });

  // Hide modal on click
  $('.video-modal-close').click(function(){
    $('.video-modal').fadeOut(700).hide();
  })






});

/*
$(window).scroll(function(){
    var wScroll = $(this).scrollTop();
    console.log(wScroll);

    $('#Screenshot').css({
      'transform' : 'translate(0px, -'+ wScroll /4 +'%)'
    });


});*/
